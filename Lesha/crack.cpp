#include <cstdio>
#include <cstdlib>
#include <cstring>

size_t count_chars(FILE* file){
	
	fseek(file, 0L, SEEK_END);
	int	num_chars = ftell(file);
	rewind(file);

	return num_chars;
}

int check_file(FILE* file){

	size_t f_len = count_chars(file);
	unsigned char* buf = (unsigned char*)calloc(f_len, sizeof(unsigned char));
	fread(buf, sizeof(unsigned char), f_len, file);
	
	if (buf[128 + 14] == 0x90){
		return 1;
	}
	
	if (buf[1] != 'E' || buf[2] != 'L' || buf[3] != 'F'){
		return 2;
	}

	return 0;

}

void change_file(FILE* input_f, FILE* output_f){

	size_t f_len = count_chars(input_f);
	unsigned char* buf = (unsigned char*)calloc(f_len, sizeof(unsigned char));

	fread(buf, sizeof(unsigned char), f_len, input_f);
	
	//cmp rax, rax
	buf[128 + 12] = 0x39;
	buf[128 + 13] = 0xc0;
	buf[128 + 14] = 0x90;

	//je <success>
	buf[128 + 15] = 0x0f;
	buf[128 + 16] = 0x84;
	buf[128 + 17] = 0xb3;

	
	//mov rsi, <success_str>
	buf[128 + 13*16 + 2] = 0x48;
	buf[128 + 13*16 + 3] = 0xc7;
	buf[128 + 13*16 + 4] = 0xc6;
	buf[128 + 13*16 + 5] = 0x0e;
	buf[128 + 13*16 + 6] = 0x04;
	buf[128 + 13*16 + 7] = 0x40;
	buf[128 + 13*16 + 8] = 0x00;
	buf[128 + 13*16 + 9] = 0x90;
	buf[128 + 13*16 + 10] = 0x90;
	buf[128 + 13*16 + 11] = 0x90;

	//mov rdx, <sucess_str_len>
	buf[128 + 13*16 + 13] = 0x92;


	fwrite(buf, sizeof(unsigned char), f_len, output_f);
	free(buf);
	
}

int main(int argc, const char* argv[]){

	printf("===================================================================\n");
	printf("\t\033[1;31m WeLcOmE tO \033[1;32m dA pAtChEr \033[1;35m 3000 \033[1;36m VeRsIoN 24.11.01\033[0m \n");
	
	if (argc - 1 != 2){
		printf("usage: %s <input> <output>\n", argv[0]);	
		return 1;
	}

	FILE* input_f  = fopen(argv[1], "r");
	FILE* output_f = fopen(argv[2], "w");

	if (input_f == nullptr){
		printf("Error: bad filename\n");
		return 2;
	}

	if (check_file(input_f) != 0){
		printf("Error: bad file\n");
		return 3;	
	}

	printf("===================================================================\n");
	printf("\t\t     Your code was patched\n\t\t(almost without bitcoin miners)\n");
	printf("===================================================================\n");

	change_file(input_f, output_f);

	fclose(input_f);
	fclose(output_f);

	return 0;
}
