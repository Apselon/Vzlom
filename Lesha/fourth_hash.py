from z3 import *

def LShL(a, i):
    return a * BitVecVal(1 << i, 32)

def hash_BitVal(key):
    h = 0
    for i in range(len(key)):
        h += key[i]
        h += LShL(h, 10)
        h ^= LShR(h, 6)

    h += LShL(h, 3)
    h ^= LShR(h, 11)
    h += LShL(h, 15)

    return simplify(h)


b = BitVecVal(0x30d1e7ee, 32);
c = BitVecVal(0x11111111, 32);
d = BitVecVal(0x00000080, 32);

a = [];
for i in range(20):
    a.append(BitVec("a_%d" % i, 32));


s = Solver()
s.add(hash_BitVal(a) ^ b == c)

for i in range(20):
    s.add(BV2Int(a[i]) < 58)
    s.add(BV2Int(a[i]) > 47)

s.check()
for i in range(20):
    print(s.model()[a[i]])
