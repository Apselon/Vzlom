
def hash(s):
    h = 0

    for i in range(len(s)):
        h += ord(s[i])
        h += h << 10
        h = h ^ (h >> 6) 

    h += h << 3
    h = h ^ h >> 11
    h += h << 15
    
    return h

