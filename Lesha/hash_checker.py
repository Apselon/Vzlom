ror = lambda val, r_bits, max_bits: \
    ((val & (2**max_bits-1)) >> r_bits%max_bits) | \
    (val << (max_bits-(r_bits%max_bits)) & (2**max_bits-1))

def check(h):
    rax = 0x55bc88bd ^ h
    rax = rax % 2**32
    assert(rax > 0x652AFFBE)
    assert(rax < 0x656DCE8A)
    
    rax = 0x5da48ace ^ h
    rax = rax % 2**32
    assert(rax > 0x6d752c2e)

    rax = h - 0x12345678
    rax = rax % 2**32
    
    assert(rax < 0x5B413322)

    rax = 0x45bb888c ^ h
    rax = rax % 2**32
    
    assert(rax > 0x756A6F11)

    rax = ror(rax, 16, 32)
    rax = rax % 2**32
    assert(rax < 0x6F8AEDA0)

    rax = 0x10be8583 ^ h
    rax = rax % 2**32
    rax = ror(rax, 32-0x18, 32)
    rax = rax % 2**32
    rax = rax >> 0x18
    rax = rax % 2**32

    assert(rax == 0x6d)

for i in range(0, 2**64 + 1):
    try:
        check(i)
        print("%x" % i)
        break
    except:
        continue
