from itertools import permutations as combinations
#from string import ascii_letters as alphabet

def jenkins(s):
    l = len(s)
    h = 0

    for i in range(0, l):
        h += ord(s[i])
        h %= 2**32
        h += (h << 10) 
        h %= 2**32
        h ^= (h >> 6)
        h %= 2**32

    h += h << 3
    h %= 2**32
    h ^= h >> 11
    h %= 2**32
    h += h << 15
    h %= 2**32

    return h 

alphabet = list(map(chr, list(range(32, 128))))

ans = 0x30d1e7ee ^ 0x11111111;

def main():
    for l in range(20, 21):
        print(l);
        combs = combinations(alphabet, l)

        for comb in combs:
            comb = "".join(comb);
            j = jenkins(comb);
           # print("%s %x" % (comb, j));
            if j == ans:
                print(comb)
                return

if __name__ == "__main__":
    main()
    
