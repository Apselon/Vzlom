from itertools import permutations as combinations

d = 0x30d1e7ee

def is_ans(c):
    rbx = 0

    for i in range(20):
        rax = ord(c[i])
        rbx += rax % 2**32
        rax = rbx % 2**32
        rax = (rax << 10)
        rbx += rax % 2**32
        rax = rbx % 2**32
        rax = rax >> 6
        rbx = (rbx ^ rax) % 2**32

    rax = rbx % 2**32
    rax = rax << 3
    rbx = rbx + rax % 2**32
    rax = rbx 
    rax = rax >> 11
    rbx = rbx ^ rax
    rax = rbx % 2**32
    rax = rax << 15
    rbx = rbx + rax

    return rbx % 2**32

alphabet = list(map(chr, list(range(1, 128))))
combs = combinations(alphabet, 20)

for comb in combs:
    comb = "".join(comb);
    if is_ans(comb) ^ d == 0x11111111:
        print(comb)
        break
