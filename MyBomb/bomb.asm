section .data
		giant_buf times 512 db 0
		big_buf times 256 db 0

		filename db "bomb"
		q1       db 10d, "Have border relations with Canada even been better?", 10d, 0
		q2       db 10d, "Здесь вам не тут!", 10d, 0
		q3		 db 10d, "I see you like this Animal Intersection game.", 10d, 0
		q4		 db 10d, "Шел Адам по улице. Нашел деньги. Посчитал. НЕ ХВАТАЕТ!", 10d, 0
		q5		 db 10d, "Поздравляю, вы выполнили все пункты: от А до Б", 10d, 0

		buf     db 0,0
		gate_1  db 0,0,0,0,0,0,0,0
		buf2    db 0,0,0,0,0,0,0,0,0,0,0,0,0

;section .bss
;		buf resb 32

section .special exec write

		global _start
		global puts
		global gets


_start:
		call protector	
		call phase_1
		call unlock_phase_2
		call phase_2
		call phase_3


		mov rsi, q5 
		call puts

		mov rax, 60d
		xor rdi, rdi
		syscall

;==============================================================================
; bufer overflow
;==============================================================================
phase_1:
		mov rsi, q1 
		call puts

		mov rax, terminate
		mov [gate_1], rax

		mov rsi, buf
		call gets

		jmp [gate_1]

phase_1_success:
		ret


;==============================================================================
; Rename the file to /usr/bin/AnimalCrossing
;==============================================================================

unlock_phase_2:
		mov rsi, locked_start
		mov rdi, locked_start

.continue:
		xor rax, rax
		lodsw
		xor rax, 3
		stosw

		cmp rsi, locked_end
		jl .continue
		
		ret

phase_2:
		mov rsi, q3 
		call puts

locked_start:
; 		mov rax, 79d
; 		mov rdi, big_buf
; 		mov rsi, 256d
; 		syscall
; 		nop
; 		nop

		mov    ebx,0x300034f
		mov    rsp,0x300030003401100
		mov    esi,0x30103
		or     al,0x5
		xchg   ebx,eax
		nop
locked_end:

		mov rsi, big_buf
		call count_simple_hash

		cmp rax, 0x52285a2c
		je phase_2_success

		jmp terminate


phase_2_success:
		ret

;==============================================================================
; Protector of the protector 
;==============================================================================
count_protector_hash:
		mov rsi, protector	
		xor r13, r13

.continue:
		xor rax, rax
		lodsb
		add r13, rax

		cmp rsi, protector_end
		jl .continue

		ret
		

;==============================================================================
; Protector + Hash
;==============================================================================
phase_3:
		mov rsi, q4 
		call puts

		call count_protector_hash
		
		mov rsi, giant_buf
		call gets

		mov rsi, giant_buf
		call count_simple_hash

		xor rax, r13
		cmp rax, 0x143da9c5
		je phase_3_success

		jmp terminate

phase_3_success:
		ret


;==============================================================================
; rsi - string to hash
; rax - hash
;==============================================================================
count_simple_hash:
		xor rax, rax

.continue:
		add rax, [rsi]
		lea eax, [eax*4]
		inc rsi
		not rax

		cmp byte [rsi], 0
		jne .continue

		lea eax, [eax*4]
		ret

;==============================================================================
; Protector
;==============================================================================
protector:
		mov rax, 65h
		xor rdi, rdi
		syscall

		cmp rax, 0
		jl terminate

protector_end:
		ret


;==============================================================================
; Exit
;==============================================================================
terminate:
		mov rsi, q2 
		call puts

		mov rax, 60d
		mov rdi, 0
		syscall

		ret
	


;==============================================================================
; In:  rsi - string to print
; Out: rdx - string len
;==============================================================================
puts:
		push rdi

		mov rax, 1
		mov rdi, 1
		mov rdx, 1

.continue:
		syscall
		inc rsi

		cmp byte [rsi], 0
		jne .continue

		mov rdx, rsi
		pop rsi
		sub rdx, rsi	

		ret


;==============================================================================
; In:  rsi - buf to input
; Out: rdx - string len
;==============================================================================
gets:
		push rdi
		
		mov rdi, 0
		mov rdx, 1

.continue:
		mov rax, 0
		syscall
		
		cmp byte [rsi], 0
		je .exit
		cmp byte [rsi], 10d
		je .exit
		
		inc rsi
		jmp .continue

.exit:
		mov rdx, rsi
		pop rsi
		sub rdx, rsi

		ret
		
		
